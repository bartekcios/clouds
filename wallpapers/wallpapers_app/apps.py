from django.apps import AppConfig


class WallpapersAppConfig(AppConfig):
    name = 'wallpapers_app'
