from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import UploadFileForm
import boto3
import os

def wallpapers(request):
    return render(request, 'wallpapers.html')

def all(request):
    client = boto3.client("s3")
    data = client.list_objects(Bucket='wallpapers-bucket-data').get("Contents", [])
    files = []
    for d in data:
        files.append(d.get('Key'))
    return render(request, 'all.html', {'files': files})

def upload_file(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = UploadFileForm(request.POST, request.FILES)
            if form.is_valid():
                client = boto3.client("s3")
                with open(request.FILES['file'].name, 'wb+') as destination:
                    for chunk in request.FILES['file'].chunks():
                        destination.write(chunk)
                client.upload_file(request.FILES['file'].name, 'wallpapers-bucket-data', request.FILES['file'].name, ExtraArgs={'ACL':'public-read'})
                return render(request, 'success.html', {'file': request.FILES['file'].name})
        else:
            form = UploadFileForm()
        return render(request, 'upload_file.html', {'form': form})
    else:
        return render(request, 'error.html')

